#!/bin/sh

qemu_args=""

while [ -n "$1" ]; do
  case $1 in
    -d)
      qemu_args="-s -S"
      ;;
    *)
      ;;
  esac
  shift
done

cargo build
if [ $? != 0 ]; then
    exit -1
fi

rust-objcopy --strip-all target/riscv64gc-unknown-none-elf/debug/myos -O binary target/riscv64gc-unknown-none-elf/debug/myos.bin

qemu-system-riscv64 -machine virt -bios default -nographic $qemu_args -kernel target/riscv64gc-unknown-none-elf/debug/myos.bin
    
