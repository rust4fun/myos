
use lazy_static::*;
use core::cell::RefCell;
use core::arch::{asm, global_asm};
use crate::sbi;


/********************************************************************
 * 
 * 切换任务的汇编函数定义：
 *      1、把寄存器信息保存到当前任务的 Context 中
 *      2、把要运行任务的寄存器信息读出来并跳转到这个任务运行
 * 核心原理：
 *      1、任务上下文中存储的最重要的是 SP 和下一条指令的 PC 地址信息
 *      2、__switch 函数执行完保存和恢复现场操作（包括 SP）
 *      3、__switch 把下一条指令的 PC 地址设置到函数返回 RA 寄存器(ARM 叫 LR)
 *      4、调用 ret 函数返回下一个任务继续执行
 * 
 ********************************************************************/
global_asm!(include_str!("switch.S"));
extern "C" {
    /* 上下文切换，当前线程保存执行状态，下一个线程恢复执行状态 */
    pub fn __switch(current_context: *mut TaskContext, next_context: *const TaskContext);
    pub fn __restore(ctx: *const TaskContext);
    pub fn __inittrap(ctx: *const TaskContext);
    
}

/********************************************************************
 * 
 * 任务上下文定义：用于保存函数切换上下文信息（寄存器以及堆栈、PC等信息）
 * 
 ********************************************************************/
#[derive(Copy, Clone, Debug)]
#[repr(C)]
pub struct TaskContext {
    pub x: [usize; 32],
    pub sstatus: usize,
    pub sepc: usize,
    pub id: usize,
}

impl TaskContext {
    pub fn new() -> Self {
        Self {
            x: [0; 32],
            sstatus: 0,
            sepc: 0,
            id: 0,
        }
    }
    pub fn init(id: usize, sp: usize, entry: usize) -> Self {
        let mut ctx = Self {
            x: [0; 32],
            sstatus: 0,
            sepc: entry,
            id: id,
        };
        /* SP 寄存器 */
        ctx.x[2] = sp;
        /* SPP 和 SPIE 设置为 1 */
        ctx.sstatus = 0x120;
        return ctx;
    }
}

use core::fmt;
impl fmt::Display for TaskContext {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "TaskContext<id:{}, PC {:x}, SP {:x}>", self.id, self.x[1], self.x[2])
    }
}

/* 封装一层结构体给 RefCell 包装起来 */
pub struct TaskManagerInner {
    pub task_context : [TaskContext; 5],
    pub current_task : usize,
}
/* TaskManager 管理 RefCell 即可 */
pub struct TaskManager {
    inner: RefCell<TaskManagerInner>,
}
/* 告诉编译器单 CPU 运行，全局变量可在不同的线程间切换；
 * 多 CPU 版本的话得用 Arc<Mutex>，这里还没涉及到
 */
unsafe impl Sync for TaskManager {}

impl TaskManagerInner {
    pub fn new() -> Self {
        Self {
            task_context: [
                TaskContext::new(),
                TaskContext::new(),
                TaskContext::new(),
                TaskContext::new(),
                TaskContext::new(),
            ],
            current_task: 0,
        }
    }
}

impl TaskManager {
    pub fn new() -> Self {
        Self {
            inner: unsafe {
                RefCell::new(TaskManagerInner::new())  
            },
        }
    }

    pub fn run_first(&self) {
        let mut task_man = self.inner.borrow_mut();
        /* 设置当前线程为 0 */
        task_man.current_task = 0;
        /* 取出第一个任务 Context */
        let first_context = &task_man.task_context[0] as *const TaskContext;;
        /* 创建一个空的 TaskContex */
        let mut _unused = &mut TaskContext::new();
        /* 打印 TaskContex ，对比 target/output.map 确认代码是否正确 */
        println!("run first task : entry {}", unsafe {*first_context});
        /* borrow_mut 需要手动释放 */
        drop(task_man);
        /* 切换到线程0 */
        unsafe {
            __switch(_unused, first_context);
        } 
    }

    pub fn next(&self) -> * const TaskContext {
        /* 获取当前任务 TaskContex 信息 */
        let mut task_man = self.inner.borrow_mut();
        let curr_idx = task_man.current_task;
        /* 获取下一个任务 TaskContex 信息 */
        let mut next_idx = curr_idx + 1;
        if next_idx >= task_man.task_context.len() {
            next_idx = 0;
        }
        task_man.current_task = next_idx;
        let next_context = &task_man.task_context[next_idx] as *const TaskContext;
        /* RefCell borrow 之后需要手动 drop */
        drop(task_man);
        return next_context;
    }

    /* 线程调度（当前线程 ID+1 即可） */
    pub fn sched(&self) {
        /* 禁用中断, __switch 恢复时使能中断 */
        unsafe { riscv::register::sstatus::clear_sie(); }
        /* 获取当前任务 TaskContex 信息 */
        let mut task_man = self.inner.borrow_mut();
        let curr_idx = task_man.current_task;
        let mut curr_context = &mut task_man.task_context[curr_idx] as *mut TaskContext;;
        /* 获取下一个任务 TaskContex 信息 */
        let mut next_idx = curr_idx + 1;
        if next_idx >= task_man.task_context.len() {
            next_idx = 0;
        }
        task_man.current_task = next_idx;
        let next_context = &task_man.task_context[next_idx] as *const TaskContext;
        // unsafe {println!("switch current {} to next {}", *curr_context, *next_context);}
        /* RefCell borrow 之后需要手动 drop */
        drop(task_man);
        /* 执行切换 */
        unsafe {
            __switch(curr_context, next_context);
        }
    }
    
}


const TASK_STACK_SIZE : usize = 4096 * 16;
#[link_section = ".bss.stack"]
#[no_mangle]
static mut TASK1_STACK: [u8; TASK_STACK_SIZE] = [0; TASK_STACK_SIZE];
#[link_section = ".bss.stack"]
#[no_mangle]
static mut TASK2_STACK: [u8; TASK_STACK_SIZE] = [0; TASK_STACK_SIZE];
#[link_section = ".bss.stack"]
#[no_mangle]
static mut TASK3_STACK: [u8; TASK_STACK_SIZE] = [0; TASK_STACK_SIZE];
#[link_section = ".bss.stack"]
#[no_mangle]
static mut TASK4_STACK: [u8; TASK_STACK_SIZE] = [0; TASK_STACK_SIZE];
#[link_section = ".bss.stack"]
#[no_mangle]
static mut TASK5_STACK: [u8; TASK_STACK_SIZE] = [0; TASK_STACK_SIZE];

lazy_static! {
    pub static ref TASK_MANAGER: TaskManager = {
        let task_man = TaskManager::new();
        {
            let mut inner = &mut task_man.inner.borrow_mut();
            let t1_stack = unsafe { TASK1_STACK.as_ptr() as usize};
            let t2_stack = unsafe { TASK2_STACK.as_ptr() as usize};
            let t3_stack = unsafe { TASK3_STACK.as_ptr() as usize};
            let t4_stack = unsafe { TASK4_STACK.as_ptr() as usize};
            let t5_stack = unsafe { TASK5_STACK.as_ptr() as usize};
            inner.task_context[0] = TaskContext::init(1, t1_stack + TASK_STACK_SIZE, task1 as usize);
            inner.task_context[1] = TaskContext::init(2, t2_stack + TASK_STACK_SIZE, task2 as usize);
            inner.task_context[2] = TaskContext::init(3, t3_stack + TASK_STACK_SIZE, task3 as usize);
            inner.task_context[3] = TaskContext::init(4, t4_stack + TASK_STACK_SIZE, task4 as usize);
            inner.task_context[4] = TaskContext::init(5, t5_stack + TASK_STACK_SIZE, task5 as usize);
        }
        task_man
    };
}

pub fn task_next() -> * const TaskContext {
    return TASK_MANAGER.next();
}

/* 线程主动退出，重新进行调度 */
pub fn task_yield() {
    TASK_MANAGER.sched();
}

/* 线程睡眠函数 */
pub fn task_sleep(ms: u64) {
    let mut curr = riscv::register::time::read() as u64;
    let target = curr + crate::trap::CLOCK_FREQ/1000 * ms;
    while (curr < target) {
        task_yield();
        curr = riscv::register::time::read() as u64;
    }
    
}

/* 线程1，打印并让出 CPU */
fn task1() {
    loop {
        println!("this is task1");
        task_sleep(1000);
    }
}

fn task2() {
    loop {
        println!("this is task2");
        task_sleep(2000);  
    }
    
}

fn task3() {
    loop {
        println!("this is task3");
        task_sleep(3000);
    }
}

fn task4() {
    loop {
        println!("this is task4");
        task_sleep(4000);
    }
    
}

fn task5() {
    let mut count = 0;
    loop {
        println!("this is task5, count - {}",count);
        task_sleep(2000);
        count = count + 1;
        // if count == 5 {
        //    sbi::shutdown(false);
        // }
    }
}

/* 运行第一个线程 */
pub fn task_run() {
    TASK_MANAGER.run_first();
}
