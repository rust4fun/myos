.altmacro
.macro SAVE_GP n
    sd x\n, \n*8(sp)
.endm
.macro LOAD_GP n
    ld x\n, \n*8(sp)
.endm
    .section .text
    .globl __inittrap
    .globl __alltraps
    .globl __restore
    .align 2

__inittrap:
    csrw sscratch, a0
    ret

    .align 2
    ### 定义中断处理函数 : __alltraps
__alltraps:
    ### scratch 保存运行线程的 TaskContext 地址; 使用 SP 交换
    csrrw sp, sscratch, sp
    ### 保存 x1~x31 寄存器到 TaskContext
    .set n, 1
    .rept 31
        SAVE_GP %n
        .set n, n+1
    .endr
    ### 保存中断状态寄存器 sstatus 和 SEPC / SP
    csrr t0, sstatus
    csrr t1, sepc
    sd t0, 32*8(sp)
    sd t1, 33*8(sp)
    csrr t2, sscratch
    sd t2, 2*8(sp)
    ### 调用到 rust 函数处理中断信息
    mv a0, sp
    mv sp, t2
    call trap_handler

__restore:
    ### 现场恢复是现场保存到逆过程，先恢复 sstatus 和 SEPC
    mv sp, a0
    ld t0, 32*8(sp)
    ld t1, 33*8(sp)
    csrw sstatus, t0
    csrw sepc, t1
    ### 设置 scratch 寄存器给下次中断使用
    csrw sscratch, a0
    ### 还原其他通用寄存器（分两步，SP 最后还原）
    ld x1, 1*8(sp)
    .set n, 3
    .rept 29
        LOAD_GP %n
        .set n, n+1
    .endr
    ### 还原 SP 寄存器并返回中断处理
    ld sp, 2*8(sp)
    sret
