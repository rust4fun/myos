# myos

#### 介绍

使用 Rust 写操作系统是一个学习项目，主要参考 [rCore-Tutorial-Book 第三版](https://rcore-os.cn/rCore-Tutorial-Book-v3/index.html)



#### 安装教程

* 开发环境：Macos

* 开发工具：rust、qemu、gcc、vscode 等

* 搭建教程：[Rust 开发环境搭建-MacOS](https://zhuanlan.zhihu.com/p/687527775)



#### Riscv64 资料

* Riscv SBI 文档：https://github.com/riscv-non-isa/riscv-sbi-doc




#### 知乎专栏

* [Rust写操作系统](https://www.zhihu.com/column/c_1752775898246602752)

