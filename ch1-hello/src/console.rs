/* 这里不使用 std，只使用 core 的定义 */
use core::fmt::{self, Write};

/* 定义一个空对象，主要是为了继承 Write 这个 trait 并继承其默认代码 */
struct Stdout;

/* 继承 Write trait */
impl Write for Stdout {
    /* 我们只需要使用这个函数即可，其他默认实现会把 format 转成 str */
    fn write_str(&mut self, s: &str) -> fmt::Result {
        for c in s.chars() {
            crate::sbi::console_putchar(c as usize);
        }
        Ok(())
    }
}

/* 定义打印函数，参数： format_args 宏的返回值 */
pub fn __print(args: fmt::Arguments) {
    Stdout.write_fmt(args).unwrap();
}

// 定义 print 宏
#[macro_export]
macro_rules! print {
    ($($arg:tt)*) => {
        $crate::console::__print(format_args!($($arg)*));
    }
}

// 定义 println 宏
#[macro_export]
macro_rules! println {
    () => { $crate::print!("\n") };
    ($($arg:tt)*) => {
        $crate::console::__print(format_args!("{}\n", format_args!($($arg)*)));
    }
}
