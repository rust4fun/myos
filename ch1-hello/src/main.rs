#![no_std]
#![no_main]
#![feature(asm_const)]

#[macro_use]
mod console;
mod sbi;

/***************************************************************
 * 
 *  程序堆栈空间定义，使用一个静态数组
 *  这个空间存放在 .bss.stack 段，可以查看 linker.ld 了解具体位置信息
 *  链接后的地址信息可以通过 target/output.map 查看
 * 
 ***************************************************************/
const TASK_STACK_SIZE : usize = 4096;
#[link_section = ".bss.stack"]
#[no_mangle]
static mut SYSTEM_STACK: [u8; TASK_STACK_SIZE] = [0; TASK_STACK_SIZE];


/***************************************************************
 * 
 *  程序 panic 时 Rust 会调用这个函数
 * 
 ***************************************************************/
use core::panic::PanicInfo;
/** `#[panic_handler]` function required, but not found */
#[panic_handler]
fn panic_handler(_info : &PanicInfo) -> ! {
    loop {}
}


/***************************************************************
 * 
 *  myos 的入口地址，由 OpenSBI 调用；
 *  _start 函数只做两件事：
 *      1、初始化堆栈(设置 SP 寄存器），以支持函数调用
 *      2、调用 rust_entry 函数
 * 
 ***************************************************************/
/** no_mangle: 要求编译起不改变函数名称 */
#[no_mangle]
/** 指定代码 section， 结合 linker.ld 可以保证 _start 函数入口地址 */
#[link_section = ".text.entry"]
/** 遵循 C 规范调用标准  */
unsafe extern "C" fn _start() -> ! {
    // OpenSBI 调用过来的第一个函数并传递两个参数：硬件ID和DTB地址
    //      PC = 0x8020_0000， a0 = hartid， a1 = dtb
    core::arch::asm!("
        la      sp, {boot_stack}
        li      t0, {boot_stack_size}
        add     sp, sp, t0              // setup boot stack
        call    {rust_entry}            // call rust_entry(hartid, dtb)                   
        j       .",
        boot_stack_size = const TASK_STACK_SIZE,
        boot_stack = sym SYSTEM_STACK,
        rust_entry = sym rust_entry,
        options(noreturn),
    )
}


/***************************************************************
 * 
 *  rust_entry 初始化 bss 段内存并打印 Hello myos
 * 
 ***************************************************************/
#[no_mangle]
unsafe extern "C" fn rust_entry(_cpu_id: usize, _dtb: usize) {
    // 导入 linker.ld 中定义的符号，最终输出的值可以在 target/output.map 文件中查看
    extern "C" { fn sbss(); fn ebss(); }
    /* 初始化 bss 段内存 */
    core::slice::from_raw_parts_mut(sbss as *mut u8, ebss as usize - sbss as usize).fill(0);

    println!("+++++++++++++++++++++++++++++++++++++++++++++++++\n\n");
    println!("     Hello myos !!!\n\n");
    println!("+++++++++++++++++++++++++++++++++++++++++++++++++\n\n");

/*  
    /* 打印字符串 */
    for ch in "Hello myos!\n".chars() {
        console::console_putchar(ch as usize);
    }
 */

    sbi::shutdown(false);

}

/* 下面代码调整到 sbi.rs 和 console.rs 中了

/***************************************************************
 * 
 *  OpenSBI 已经提供了打印串口输出的函数调用，不需要自己写串口驱动了
 *  通过 ecall 指令陷入到 OpenSBI，通过汇编把打印的字符串传入即可
 * 
 ***************************************************************/
 pub fn console_putchar(c: usize) -> usize {
    pub const LEGACY_CONSOLE_PUTCHAR: usize = 1;
    sbi_ecall1(LEGACY_CONSOLE_PUTCHAR, c)
}

/***************************************************************
 *  通过 ecall 指令陷入到 OpenSBI，只需指定 extendid 和 arg0 参数
 ***************************************************************/
fn sbi_ecall1(eid: usize, arg0: usize) -> usize {
    let error;
    unsafe {
        core::arch::asm!(
            "ecall",
            in("a7") eid,
            inlateout("a0") arg0 => error,
        );
    }
    error
}

*/