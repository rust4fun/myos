
/***************************************************************
 * 
 *  OpenSBI 已经提供了打印串口输出的函数调用，不需要自己写串口驱动了
 *  通过 ecall 指令陷入到 OpenSBI，通过汇编把打印的字符串传入即可
 * 
 ***************************************************************/
 pub fn console_putchar(c: usize) {
    #[allow(deprecated)]
    sbi_rt::legacy::console_putchar(c);
}

/***************************************************************
 *  OpenSBI 关机命令
 ***************************************************************/
 pub fn shutdown(failure: bool) -> ! {
    use sbi_rt::{system_reset, NoReason, Shutdown, SystemFailure};
    if !failure {
        system_reset(Shutdown, NoReason);
    } else {
        system_reset(Shutdown, SystemFailure);
    }
    unreachable!()
}
