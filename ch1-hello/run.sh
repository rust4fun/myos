#!/bin/sh

args="-s -S"

cargo build
if [ $? != 0 ]; then
    exit -1
fi

rust-objcopy --strip-all target/riscv64gc-unknown-none-elf/debug/myos -O binary target/riscv64gc-unknown-none-elf/debug/myos.bin

qemu-system-riscv64 -machine virt -bios default -nographic -kernel target/riscv64gc-unknown-none-elf/debug/myos.bin
    