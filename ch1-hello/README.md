## Rust 写操作系统之 Hello world

### 文章说明

* [开发环境搭建 - MacOS](https://zhuanlan.zhihu.com/p/687513515)
* [Rust 写操作系统之 Hello world （一）](https://zhuanlan.zhihu.com/p/687515644)
* [Rust 写操作系统之 Hello world（二）](https://zhuanlan.zhihu.com/p/687516665)
* [Rust 写操作系统之 Hello world（三）](https://zhuanlan.zhihu.com/p/687525445)

### 编译运行

~~~sh
### rust 编译
cargo build

### 生成二进制文件
rust-objcopy --strip-all target/riscv64gc-unknown-none-elf/debug/myos -O binary target/riscv64gc-unknown-none-elf/debug/myos.bin

### 运行 Qemu 虚拟机
qemu-system-riscv64 -machine virt -bios default -nographic -kernel target/riscv64gc-unknown-none-elf/debug/myos.bin
~~~



这里感谢 [rCore-Tutorial-Book 第三版](https://rcore-os.cn/rCore-Tutorial-Book-v3/index.html) 项目的人员，从中学到了很多知识。

