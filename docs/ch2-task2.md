

## Rust 写操作系统之线程（三）

Rust 写操作系统系列：

[Rust 写操作系统之线程（一）](https://zhuanlan.zhihu.com/p/687728867)

[Rust 写操作系统之线程（二）](https://zhuanlan.zhihu.com/p/688549135)

上一章我们介绍了线程协商式的任务调度方法，这一章我们开始来实现抢占式线程调度。

说到抢占，我们最容易想到的就是分时抢占机制了（每个线程执行轮流执行10ms）。

为了实现线程的分时执行，我们得有一个定时器，每隔 10ms 告诉我们得换一个线程来运行了。

说到定时器我们又不得不提一下 CPU 中断了。中断是计算机系统中的一种重要机制，用于处理各种异步事件和异常情况。当发生中断时，CPU 会暂停当前正在执行的指令流，跳转到预定义的中断处理程序，执行与中断相关的处理逻辑，然后返回到原来的执行流中。以下是 CPU 中断的基本概念和工作原理：

1. **中断来源：** 中断可以由各种异步事件触发，例如设备完成了数据传输、定时器到期、硬件故障、用户输入等。每个中断都会有一个唯一的标识符，称为中断向量或中断号。
2. **中断向量表：** 中断向量表是一个存储中断处理程序入口地址的数据结构，通常是一个数组或者表格。每个中断号都对应着一个中断处理程序的入口地址，CPU根据中断号查找中断向量表，然后跳转到对应的处理程序。
3. **中断处理过程：** 当发生中断时，CPU会完成以下几个步骤：
   - 保存当前的上下文信息，包括程序计数器、寄存器状态等。
   - 根据中断号查找中断向量表，获取对应的中断处理程序的入口地址。
   - 跳转到中断处理程序的入口地址，执行与中断相关的处理逻辑。
   - 中断处理程序执行完毕后，恢复之前保存的上下文信息，然后返回到原来的执行流中继续执行。
4. **中断优先级：** 不同的中断可能有不同的优先级，通常情况下，高优先级的中断可以打断低优先级的中断处理程序。处理器通常会提供中断屏蔽机制，允许软件设置和管理中断的优先级和屏蔽条件。

说到这里，想必大家基本已经知道，我们这一章就是要先实现一个定时器中断处理过程，至于如何进行线程抢占调度我们可以放到后面再考虑。了解过 CPU 体系结构或者学习过单片机的同学都知道，各类 CPU 处理中断的流程都大同小异。我们要实现定时器中断处理函数，通用的步骤如下：

* 设置中断向量表：这里我们定义一个定时器中断处理函数，并把这个函数设置到中断向量表中
* 设置定时器比较器/计数器时钟源等寄存器信息（要查 CPU 的寄存器手册了）
* 开启 CPU 全局中断
* 开启定时器中断、开启定时器（每种类型的中断源都有独立的使能开关）

好了，我先救按这个步骤来实现 Riscv64 平台的定时器处理函数吧。

### 中断处理函数

![regs](riscv-regs.jpeg)

线程在正常执行过程中被中断给打断并跳转到中断处理函数，我们首先要做的就是保存线程的上下文（是不是感觉很熟悉）。没错，就跟我们上一章讲的上下文切换类似，我们中断处理函数也得做上下文的保存跟恢复。要完成这件事，还是得上汇编（保存寄存器到堆栈，调用rust中断处理函数，还原寄存器并完成中断返回）：

~~~sh
.altmacro
.macro SAVE_GP n
    sd x\n, \n*8(sp)
.endm
.macro LOAD_GP n
    ld x\n, \n*8(sp)
.endm
    .section .text
    .globl __alltraps
    .globl __restore
    .align 2

    ### 定义中断处理函数 : __alltraps
__alltraps:
    ### 上下文信息保存到现有堆栈上（ TrapContext 结构体大小 34 * 8 ）
    addi sp, sp, -34*8
    ### 保存 x1~x31 寄存器（调用宏）
    .set n, 1
    .rept 29
        SAVE_GP %n
        .set n, n+1
    .endr
    ### 保存中断状态寄存器 sstatus 和 PC
    csrr t0, sstatus
    csrr t1, sepc
    sd t0, 32*8(sp)
    sd t1, 33*8(sp)
    ### 调用到 rust 函数处理中断信息
    call trap_handler

__restore:
    ### 现场恢复是现场保存到逆过程，先恢复 sstatus 和 PC
    ld t0, 32*8(sp)
    ld t1, 33*8(sp)
    csrw sstatus, t0
    csrw sepc, t1
    ### 还原其他通用寄存器（注：不能动 SP 寄存器）
    ld x1, 1*8(sp)
    .set n, 3
    .rept 29
        LOAD_GP %n
        .set n, n+1
    .endr
    ### 释放堆栈上的 TrapContext 数据
    addi sp, sp, 34*8
    ### 中断恢复返回
    sret
~~~

Rust 中断处理函数要做的时候就是打印出当前时间，并重新设置下一次中断的时间。

下面代码主要是读取 Riscv64 中断相关寄存器并调用 sbi_rt 重新设置定时器中断。

~~~sh
pub const CLOCK_FREQ: u64 = 12500000;
pub const TICKS_PER_SEC: u64 = 100;

#[no_mangle]
pub extern "C" fn trap_handler() {
    /* 读取中断原因以及相应的中断值 */
    let scause = riscv::register::scause::read();
    let stval = riscv::register::stval::read();
    match scause.cause() {
        /* 定时器中断处理 */
        Trap::Interrupt(Interrupt::SupervisorTimer) => {
            /* 读取当前定时器计数 */
            let curr = riscv::register::time::read() as u64;
            /* 设置下一次中断计数值: 10ms */
            sbi_rt::set_timer(curr + CLOCK_FREQ/TICKS_PER_SEC);
            /* 打印当前计数器的值 */
            println!("[Intterupt] Current Time: {}", curr);
        }
        _ => {
            panic!("Unsupported trap {:?}, stval = {:#x}!", scause.cause(), stval);
        }
    }
}
~~~

`备注：跟汇编函数调用的 ABI 要按照 C 函数调用约定，Rust本身没有稳定 ABI 。`



### 中断注册

Riscv 中断注册比较简单，把函数处理地址写到中断向量表寄存器即可。这里不在过多介绍了，看代码：

~~~sh
pub fn init_trap() {
    extern "C" { fn __alltraps(); }
    unsafe {
        // 设置中断处理函数 
        riscv::register::stvec::write(__alltraps as usize, TrapMode::Direct);
    }
}
~~~

### 开启中断

定时器属于 CPU 内部的一个控制器，寄存器虽然不多，但是一个个对过去也得花点时间。Riscv 设置定时器可以直接调用 OpenSBI 接口（很方便，省去了看寄存器手册了）。开启中断一般就是找到中断控制寄存器，把其中一个 bit 设置为 1 就算开启了。

~~~
    // 调用 sbr_rt 设置定起始比较器触发中断
    sbi_rt::set_timer(0);
    unsafe {
        // 启动时钟中断
        riscv::register::sie::set_stimer();
        // 启用中断
        riscv::register::sstatus::set_sie();
    }
~~~



### 验收时刻

上述汇编源码地址：https://gitee.com/rust4fun/myos/blob/master/ch2-task/src/trap.S

上述中断处理 Rust 源码：https://gitee.com/rust4fun/myos/blob/master/ch2-task/src/trap.rs

源码都是编译过可直接运行的，除了依赖 riscv 和 sbi  crate 之外无其他模块耦合。添加依赖在 cargo.tom 中添加下面几行：

~~~sh
[dependencies]
sbi-rt = { version = "0.0.3", features = ["legacy"] }
lazy_static = { version = "1.4.0", features = ["spin_no_std"] }
riscv = { git = "https://github.com/rcore-os/riscv", features = ["inline-asm"] }
~~~

最后我们需要在 main.rs 中调用 trap.rs 中 init_trap 函数来触发定时器处理：

~~~sh
/* 导入 trap.rs 模块 */
mod trap;

/***************************************************************
 * 
 *  rust_entry 初始化 bss 段内存并打印 Hello myos
 * 
 ***************************************************************/
#[no_mangle]
unsafe extern "C" fn rust_entry(_cpu_id: usize, _dtb: usize) {
    clear_bss();
    
    println!("+++++++++++++++++++++++++++++++++++++++++++++++++\n\n");
    println!("     Hello myos !!!\n\n");
    println!("+++++++++++++++++++++++++++++++++++++++++++++++++\n\n");

    trap::init_trap();
    loop{}
    sbi::shutdown(false);
}
~~~

执行项目中的 `run.sh` 脚本编译 rust 并运行 qemu 即可看到一下输出：

![ch2-task2-output.jpg](ch2-task2-output.jpg)

好了，今天就先到这里了，有疑问的同学请留言。下一章我们得考虑如何在中断处理函数里面去切换线程进行真正的抢占调度了，有兴趣的同学可以先思考一下如何实现吧。

### 参考文章

* 实现特权级的切换 (https://rcore-os.cn/rCore-Tutorial-Book-v3/chapter2/4trap-handling.html)
* 详解Riscv 中断 https://www.cnblogs.com/harrypotterjackson/p/17548837.html
