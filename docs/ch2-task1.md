## Rust 写操作系统之线程（二）

书接上回 [Rust 写操作系统之线程（一）](https://zhuanlan.zhihu.com/p/687728867) 我们了解了线程和函数的区别是线程支持上下文切换，而函数没有上下文切换相关的支持。上下文切换时需要保存线程的当前执行状态：包括指令、堆栈（函数调用栈）以及寄存器相关信息。因此我们需要根据保存状态设计线程的结构体：

~~~sh
#[derive(Copy, Clone, Debug)]
#[repr(C)]
pub struct TaskContext {
    pub ra: usize,          // 下一条指令地址
    pub sp: usize,          // 线程堆栈地址
    pub s: [usize; 12],     // 线程寄存器信息
}
~~~

寄存器相关说明参考：[RISC-V通用寄存器及函数调用规范](https://lgl88911.github.io/2021/02/15/RISC-V%E9%80%9A%E7%94%A8%E5%AF%84%E5%AD%98%E5%99%A8%E5%8F%8A%E5%87%BD%E6%95%B0%E8%B0%83%E7%94%A8%E8%A7%84%E8%8C%83/)

我们按照上述约定由 callee 保存的寄存器信息，caller 保存的已经存储在堆栈内存的中了，没有必要再保存一次。除了寄存器之外，我们还需要保存指令和堆栈信息，这两个不仅仅是一个地址而已，其代表的一段内存信息。不同线程之间的指令空间（函数）是可以复用或者共享的，而堆栈不同的点在于，每个线程的函数调用到哪里了都不一样，所以上下文切换的时候需要保存堆栈对应的内存空间，而不仅仅是一个 SP 寄存器。难道我们需要在上下文切换的时候去复制堆栈内存到 TaskContext 结构体中么？

当然不是，复制内存的效率太低了，我们只需要为每个线程创建一个独立的堆栈空间，上下文切换的时候去修改 SP 寄存器即可。堆栈空间的定义也比较简单，为每个线程定义全局数组即可。

~~~sh
const TASK_STACK_SIZE : usize = 4096;
#[link_section = ".bss.stack"]
#[no_mangle]
static mut TASK1_STACK: [u8; TASK_STACK_SIZE] = [0; TASK_STACK_SIZE];
#[link_section = ".bss.stack"]
#[no_mangle]
static mut TASK2_STACK: [u8; TASK_STACK_SIZE] = [0; TASK_STACK_SIZE];
#[link_section = ".bss.stack"]
#[no_mangle]
static mut TASK3_STACK: [u8; TASK_STACK_SIZE] = [0; TASK_STACK_SIZE];
#[link_section = ".bss.stack"]
#[no_mangle]
static mut TASK4_STACK: [u8; TASK_STACK_SIZE] = [0; TASK_STACK_SIZE];
#[link_section = ".bss.stack"]
#[no_mangle]
static mut TASK5_STACK: [u8; TASK_STACK_SIZE] = [0; TASK_STACK_SIZE];
~~~

上面把线程切换的理论都讲通了，让我们来修改一下代码来实现真正的线程支持吧（这一章为了说明原理，只实现了线程主动让出CPU进行调度）。

#### 线程调度

首先要实现的是线程调度方法，这个方法需要用汇编代码来实现。调度功能的实现也比较简单：

* 保存当前线程的上下文信息
* 恢复下一个线程的上下文信息
* Ret 指令返回到下一个线程继续执行

我们把这个函数命名为：__switch ，函数签名信息如下：

~~~sh
extern "C" {
    /* 上下文切换，当前线程保存执行状态，下一个线程恢复执行状态 */
    pub fn __switch(current_context: *mut TaskContext, next_context: *const TaskContext);
}
~~~

汇编代码实现如下，有兴趣的可以阅读以下，代码尽量都写注释了。

~~~sh
### 开启汇编宏定义功能
.altmacro
### 定义保存 callee 寄存器值的宏（\n 会被实际值替换）
.macro SAVE_SN n
    sd s\n, (\n+2)*8(a0)
.endm
### 定义恢复 callee 寄存器值的宏
.macro LOAD_SN n
    ld s\n, (\n+2)*8(a1)
.endm
### 定义为 代码段
    .section .text
### 定义全局函数
    .globl __switch
### 定义函数地址
# __switch(
#     current_context: *mut TaskContext, // 寄存器 a0
#     next_context: *const TaskContext   // 寄存器 a1
# )
__switch:
    ########################################
    # 保存当前线程执行状态，a0 : current_context
    ########################################
    # 保存当前线程堆栈地址
    sd sp, 8(a0)
    # 保存当前线程下一条指令地址
    sd ra, 0(a0)
    # 循环调用宏保存寄存器 s0~s11
    .set n, 0
    .rept 12
        SAVE_SN %n
        .set n, n+1
    .endr

    ########################################
    # 恢复线程执行状态：a1 : next_context
    ########################################
    # 循环调用宏恢复下个线程寄存器 s0~s11
    .set n, 0
    .rept 12
        LOAD_SN %n
        .set n, n + 1
    .endr
    # 恢复下个线程的下一条指令地址
    ld ra, 0(a1)
    # 恢复下个线程的堆栈地址
    ld sp, 8(a1)
    # 返回跳转到下个线程运行
    ret
~~~

#### 线程初始化

由于线程需要进行任务切换，我们需要修改线程的信息，我们的 TaskManager（线程管理器）需要全局可以修改的。Rust 语言不建议我们使用全局可变的变量，强行使用的话得一堆unsafe，还得自己保证变量的竞争性。不过 Rust 也提供了一种引用计数的手段让我们控制全局变量内部的可读写性（RefCell）。我们按照下面代码修改结构体后即可使得 TaskManager 全局变量不不变，需要修改的 TaskContext 的时候再通过 RefCell.borrow_mut() 来获取其可变引用。

~~~sh
use core::cell::RefCell；
/* 封装一层结构体给 RefCell 包装起来 */
pub struct TaskManagerInner {
    pub task_context : [TaskContext; 5],
    pub current_task : usize,
}
/* TaskManager 管理 RefCell 即可 */
pub struct TaskManager {
    inner: RefCell<TaskManagerInner>,
}
/* 告诉编译器单 CPU 运行，全局变量可在不同的线程间切换；
 * 多 CPU 版本的话得用 Arc<Mutex>，这里还没涉及到
 */
unsafe impl Sync for TaskManager {}
~~~

任务管理的结构体也定义好了，我们还需要使用 lazy_static 宏来初始化我们的线程。

在 Cargo.toml 中添加依赖：

~~~sh
[dependencies]
lazy_static = { version = "1.4.0", features = ["spin_no_std"] }
~~~

在 task.rs 中添加初始化代码：

~~~sh
lazy_static! {
    pub static ref TASK_MANAGER: TaskManager = {
        let task_man = TaskManager::new();
        {
            let mut inner = &mut task_man.inner.borrow_mut();
            // 设置线程的堆栈空间和运行的函数地址
            let t1_stack = unsafe { TASK1_STACK.as_ptr() as usize};
            inner.task_context[0] = TaskContext::init(t1_stack + TASK_STACK_SIZE, task1 as usize);
            // ... 其他线程不再同上
        }
        task_man
    };
}
~~~



#### 第一个线程运行

第一个线程的运行总是不一样的，我们需要特别说明一下（谁叫他是第一个呢）。特别的地方在于在运行第一个线程之前，没有当前线程的上下文，因此需要创建一个空的上下文，然后才调用 __switch 函数进行切换。函数如下：

~~~sh
impl TaskManager {
    pub fn run_first(&self) {
        let mut task_man = self.inner.borrow_mut();
        /* 设置当前线程为 0 */
        task_man.current_task = 0;
        /* 取出第一个任务 Context */
				let first_context = &task_man.task_context[0] as *const TaskContext;;
        /* 创建一个空的 TaskContex */
        let mut _unused = &mut TaskContext::new();
        /* 打印 TaskContex ，对比 target/output.map 确认代码是否正确 */
        println!("run first task : entry {}", unsafe {*first_context});
        /* borrow_mut 需要手动释放 */
        drop(task_man);
        /* 切换到线程0 */
        unsafe {__switch(_unused, first_context);} 
    }
~~~



#### 线程调度

我们之前已经上下文切换函数，线程调度只需要知道当前线程的 Context 以及下一个要运行线程的 Context 即可。为了简单期间，我们不研究什么调度算法，只要根据 TaskManager 中保存的 线程 ID（current_task）来获取当前线程上下文，ID+1 即是下个要运行的线程。代码如下：

~~~sh
impl TaskManager {
    /* 线程调度（当前线程 ID+1 即可） */
    pub fn sched(&self) {
        /* 获取当前任务 TaskContex 信息 */
        let mut task_man = self.inner.borrow_mut();
        let curr_idx = task_man.current_task;
        let mut curr_context = &mut task_man.task_context[curr_idx] as *mut TaskContext;;
        /* 获取下一个任务 TaskContex 信息 */
        let mut next_idx = task_man.current_task + 1;
        if next_idx >= task_man.task_context.len() {
            next_idx = 0;
        }
        let next_context = &task_man.task_context[next_idx] as *const TaskContext;
        task_man.current_task = next_idx;
        println!("switch current {} to next {}", curr_idx, next_idx);
        /* RefCell borrow 之后需要手动 drop */
        drop(task_man);
        /* 执行切换 */
        unsafe {__switch(curr_context, next_context);}
    }
~~~



#### 线程运行

目前为止，我们已经把线程调度相关的基本功能已经全部实现完毕。只需要写用例来测试一下理论是否可行。线程用例代码调整点如下：

* 每个线程加 loop 执行（打印一行消息后让出 CPU 给其他线程，等待下一次调度）
* 线程5 增加一个计数器，执行5次后关机（防止刷屏）
* task_run 函数调用 `TASK_MANAGER.run_first()` 运行第一个线程。

~~~sh
/* 线程主动退出，重新进行调度 */
pub fn task_yield() {
    TASK_MANAGER.sched();
}
/* 线程1，打印并让出 CPU */
fn task1() {
    loop {
        println!("this is task1");
        task_yield()
    }
}
.... 其他线程都一样，这里就不在罗列了。
fn task5() {
    let mut count = 0;
    loop {
        println!("this is task5");
        count = count + 1;
        if count == 5 {
            sbi::shutdown(false);
        } 
        task_yield();
    }
}
/* 运行第一个线程 */
pub fn task_run() {
    TASK_MANAGER.run_first();
}
~~~

完成上述代码执行 run.sh 运行查看结果如下：

![ch2-task1](ch2-task1.png)

#### 源码说明

本文相应的代码都比较简单，尽量不使用高级的封装技巧，只为了写出简单代码方便大家理解线程的原理。源码如下：

* 调度项目地址：https://gitee.com/rust4fun/myos/tree/master/ch2-task

* 本文涉及到的源码:  [task.rs](https://gitee.com/rust4fun/myos/blob/master/ch2-task/src/tasks.rs)

* 上下文切换汇编源码: [switch.S](https://gitee.com/rust4fun/myos/blob/master/ch2-task/src/switch.S)

我们已经实现了协商式的任务调度（主动放弃CPU），下个章节我们将准备开启抢占式任务调度之旅。