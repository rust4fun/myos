# Rust 写操作系统之 Hello world（二）

[Rust 写操作系统之Hello world （一）](https://zhuanlan.zhihu.com/p/687515644) 上篇文章我们已经完成了 myos 的程序编译，接下来我们要实现串口到输出。在输出串口之前我们需要初始化软件运行环境：设置堆栈以及初始化 bss 段。学过 C 语言的同学们都知道，函数调用以及函数内部的变量都存放在栈上且栈是自顶向下增长的。bss 段是程序的全局静态变量区，没有设置默认值或者默认值为0的全局变量都会存放在 bss 区域。

# 设置堆栈

由于 OpenSBI 加载我们的 myos 时没有给我们分配堆栈空间，需要我们自己去定义个对战空间并设置 SP 寄存器（指向堆栈的高地址）；下面我们在 src/main.rs 中定义个 4K 大小的堆栈并在 _start 函数中设置堆栈然后调用 rust_entry 函数。

```
/***************************************************************
 * 
 *  程序堆栈空间定义，使用一个静态数组
 *  这个空间存放在 .bss.stack 段，可以查看 linker.ld 了解具体位置信息
 *  链接后的地址信息可以通过 target/output.map 查看
 * 
 ***************************************************************/
const TASK_STACK_SIZE : usize = 4096;
#[link_section = ".bss.stack"]
#[no_mangle]
static mut SYSTEM_STACK: [u8; TASK_STACK_SIZE] = [0; TASK_STACK_SIZE];


/***************************************************************
 * 
 *  myos 的入口地址，由 OpenSBI 调用；
 *  _start 函数只做两件事：
 *      1、初始化堆栈(设置 SP 寄存器），以支持函数调用
 *      2、调用 rust_entry 函数
 * 
 ***************************************************************/
/** no_mangle: 要求编译起不改变函数名称 */
#[no_mangle]
/** 指定代码 section， 结合 linker.ld 可以保证 _start 函数入口地址 */
#[link_section = ".text.entry"]
/** 遵循 C 规范调用标准  */
unsafe extern "C" fn _start() -> ! {
    // OpenSBI 调用过来的第一个函数并传递两个参数：硬件ID和DTB地址
    //      PC = 0x8020_0000， a0 = hartid， a1 = dtb
    core::arch::asm!("
        la      sp, {boot_stack}
        li      t0, {boot_stack_size}
        add     sp, sp, t0              // setup boot stack
        call    {rust_entry}            // call rust_entry(hartid, dtb)                   
        j       .",
        boot_stack_size = const TASK_STACK_SIZE,
        boot_stack = sym SYSTEM_STACK,
        rust_entry = sym rust_entry,
        options(noreturn),
    )
}
```

这里大家可以思考一个问题：如果不设置堆栈的话会有什么问题？

答案：如果不设置堆栈的话我们是不能调用任何函数的，在 _start 函数内使用任何局部变量。OpenSBI 跳转到 _start 函数的时候并没有设置堆栈（或者说 OpenSBI 空间的堆栈我们无法使用），所以 _start 是一个环境不齐全的函数（SP寄存器不可用），我们要尽快设置 SP 之后跳转到正常的函数中(rust_entry)。

# BSS 段清零

清零 BSS 段我们需要有 BSS 的地址区间，该如何定义并获取这个信息呢。

上一章我们的 linker.ld 里面已经定义了 bss 段的区间并设置了两个变量：sbss, ebss

```
// 这里在重复贴出来讲解一下
	.bss : { 								// 定义 .bss 段
        *(.bss.stack)			// 这个是我们上面设置的堆栈区间（不能清零）
        sbss = .;					// 定义 bss 的起始地址符号
        *(.bss .bss.*)		// 存储的 section 
        *(.sbss .sbss.*)
    }
		. = ALIGN(4K);				// bss 空间 4K 对齐
    ebss = .;							// 定义 bss 的结束地址符号
```

看到 linker.ld 里面定义了 bss 内存区间的起始和结束地址定义。在 Rust 代码中，我们通过 ***extern "C" { fn sbss(); fn ebss(); }*** 来导出这两个符号信息。至于清零的方法可以用 for 循环或者 Rust 核心库提供的***
core::slice::from_raw_parts_mut(addr, size).fill(0)*** 填充函数。 我们只需要在 rust_entry 代码中通过如下代码实现清零 bss 区间。

```
/***************************************************************
 * 
 *  rust_entry 初始化 bss 段内存
 * 
 ***************************************************************/
#[no_mangle]
unsafe extern "C" fn rust_entry(_cpu_id: usize, _dtb: usize) {
    // 导入 linker.ld 中定义的符号，最终输出的值可以在 target/output.map 文件中查看
    extern "C" { fn sbss(); fn ebss(); }
    /* 初始化 bss 段内存 */
    core::slice::from_raw_parts_mut(sbss as *mut u8, ebss as usize - sbss as usize).fill(0);
		loop {}
}
```



# Riscv 串口输出

写过驱动或者玩过单片机的同学都知道，往串口输出字符串要么自己通过 GPIO 模拟 UART 串口协议，要么通过设置 CPU 内置的串口控制器实现串口输出信息。现在的 CPU 基本都集成了各种外设（串口也算是外设中的一种），大部分时候只需对着 CPU 的寄存器手册一顿操作就可以往串口输出信息了（这个也是挺麻烦的）。好在 Riscv SBI 已经实现了串口输出的标准调用（SBI 可以不精确的理解为 BIOS），我们只需要按照约定进行一个函数调用就可以输出一个字符串到串口上了。调用 SBI 函数有点跟应用程序调用内核的方法一样，按照约定设置好寄存器之后，通过 ecall 指令陷入到 SBI 执行（SBI 有自己的堆栈和地址空间，不能直接通过函数调用进入）。下面代码展示了 console_putchar 函数是如何调用 SBI 输出字符串。

```
/***************************************************************
 * 
 *  OpenSBI 已经提供了打印串口输出的函数调用，不需要自己写串口驱动了
 *  通过 ecall 指令陷入到 OpenSBI，通过汇编把打印的字符串传入即可
 * 
 ***************************************************************/
 pub fn console_putchar(c: usize) -> usize {
    pub const LEGACY_CONSOLE_PUTCHAR: usize = 1;
    sbi_ecall1(LEGACY_CONSOLE_PUTCHAR, c)
}

/***************************************************************
 *  通过 ecall 指令陷入到 OpenSBI，只需指定 extendid 和 arg0 参数
 ***************************************************************/
fn sbi_ecall1(eid: usize, arg0: usize) -> usize {
    let error;
    unsafe {
        core::arch::asm!(
            "ecall",
            in("a7") eid,
            inlateout("a0") arg0 => error,
        );
    }
    error
}
```



# 终极决战

到目前为止我们完成了以下两点任务：

1、myos 的编译运行同时也准备好了程序运行的环境。

2、在 Riscv 下输出一个字符串到串口到方法。

最后我们只需组合起来就可以完成操作系统的 Hello world 输出任务了。

```
/***************************************************************
 * 
 *  rust_entry 初始化 bss 段内存并打印 Hello myos
 * 
 ***************************************************************/
#[no_mangle]
unsafe extern "C" fn rust_entry(_cpu_id: usize, _dtb: usize) {
    // 导入 linker.ld 中定义的符号，最终输出的值可以在 target/output.map 文件中查看
    extern "C" { fn sbss(); fn ebss(); }
    /* 初始化 bss 段内存 */
    core::slice::from_raw_parts_mut(sbss as *mut u8, ebss as usize - sbss as usize).fill(0);
  
    /* 打印字符串 */
    for ch in "Hello myos!\n".chars() {
        console_putchar(ch as usize);
    }
}
```

代码都编写完了，下面执行命令见证奇迹的时候到了。

```
### rust 编译
cargo build
### 生成二进制文件
rust-objcopy --strip-all target/riscv64gc-unknown-none-elf/debug/myos -O binary target/riscv64gc-unknown-none-elf/debug/myos.bin
### 运行 Qemu 虚拟机
qemu-system-riscv64 -machine virt -bios default -nographic -kernel target/riscv64gc-unknown-none-elf/debug/myos.bin
```

看到下图的输出，我们的任务就完成了。本文相关代码都已经上传到 Gitee 上了，有兴趣的朋友可以下载运行即可：
https://gitee.com/rust4fun/myos/tree/master/ch1-hello

![img](https://p3-sign.toutiaoimg.com/tos-cn-i-axegupay5k/9f7ae3a80c7b4e63972eaaedd561f270~noop.image?_iz=58558&from=article.pc_detail&lk3s=953192f4&x-expires=1711282468&x-signature=HIF%2FjFBkYHh6QmTZ6CGgSNmFyEc%3D)

