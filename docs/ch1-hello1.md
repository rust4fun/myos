# Rust 写操作系统之 Hello world （一）

[Rust 开发环境搭建](https://zhuanlan.zhihu.com/p/687513515) 完成了，我们开始使用 Rust 编写一个属于自己的操作系统。

在正式编写操作系统之前，我们按惯例先来个 Hello world 的系统代码。

在嵌入式裸机上面输出 Hello world 并不是一件简单的事，需要完成两个条件：

1、完成裸机程序编译（不依赖 Linux libc 库）并生成二进制可执行文件

2、完成串口驱动输出（print 实际是打印到串口上）

下面我们分步完成上面两个步骤，并在 Qemu 上打印出 Hello world 字符。

这篇文章先完成第一步：程序的编译及运行（万事开头难）。

# 新建 myos 工程

```
### 使用 cargo 命令新建一个名字为 myos 的工程
cargo new myos 
    Creating binary (application) `myos` package
```

在本机上编译输出 Hello world（这个比较简单）

```
cd myos
cargo run
   Compiling myos v0.1.0 (myos)
    Finished `dev` profile [unoptimized + debuginfo] target(s) in 5.68s
     Running `target/debug/myos`
Hello, world!
```

查看 cargo new 命令生成的默认源码 src/main.rs

```
fn main() {
    println!("Hello, world!");
}
```



# 裸机编译支持

上述 myos 工程在 PC 端编译运行正常，说明我们的 Rust 安装环境都正常了。

嵌入式系统编译不能依赖 rust std，所以我们需要调整代码以支持嵌入式编译。

1、新建 .cargo/config.toml 添加 riscv64 目标编译器支持

```
### 设置默认 target, 编译时不用指定目标了
[build]
target = "riscv64gc-unknown-none-elf"

### 设置 riscv64 使用的链接器以及编译参数
### src/linker.ld 告诉链接器如何按照指定的内存布局输出程序
### -Map 输出 output.map 定位符号方便调试（gdb）
[target.riscv64gc-unknown-none-elf]
linker = "riscv64-unknown-elf-ld"
rustflags = [
    "-Clink-arg=-Tsrc/linker.ld", 
    "-Cforce-frame-pointers=yes",
    "-Clink-arg=-Map=target/output.map"
]
```



2、修改源码 main.rs，由于 main 和 println! 函数都需要 std 支持，所以嵌入式编译需要去掉，并添加 _start 函数，作为整个程序的入口。

```
#![no_std]
#![no_main]

use core::panic::PanicInfo;

/** `#[panic_handler]` function required, but not found */
#[panic_handler]
fn panic_handler(_info : &PanicInfo) -> ! {
    loop {}
}

/** no_mangle: 要求编译起不改变函数名称 */
#[no_mangle]
/** 指定代码 section， 结合 linker.ld 可以保证 _start 函数入口地址 */
#[link_section = ".text.entry"]
/** 遵循 C 规范调用标准  */
pub extern "C" fn _start() -> ! {
    loop {}
}
```



3、添加程序链接文件。这里需要说明的是，普通的主机程序编译时 GCC 内置了一个链接器 map 文件指定函数的入口函数以及地址信息（跟操作系统相关）。我们写操作系统的地址布局信息需要手动指定，系统编译的布局信息是根据 CPU bootloader 约定的一致即可。这里使用 Qemu riscv64 模拟器，bootloader 会把内核系统加载到 0x80200000 这个地址去运行。链接器 src/linker.ld 文件如下（在 .cargo/config.toml 中传递给链接器）

```
/* riscv 体系结构 */
OUTPUT_ARCH(riscv)

/* main.rs 中入口函数 */
ENTRY(_start)

/* 程序的起始地址（ bootloader 会把内核加载到这个地址运行） */
BASE_ADDRESS = 0x80200000;

SECTIONS
{
    /* 定义整个程序的起始地址为 0x80200000 */
    . = BASE_ADDRESS;

    /* 定义 skernel 符号，表示 kenrle 的起始地址 */
    skernel = .;

    /* 定义 stext 符号，表示程序区的起始地址 */
    stext = .;
    .text : {
        *(.text.entry)
        *(.text .text.*)
    }

    /* 程序区大小 4k 对齐 */
    . = ALIGN(4K);

    /* 程序区结束符号 .etext : end of text */
    etext = .;

    srodata = .;
    .rodata : {
        *(.rodata .rodata.*)
        *(.srodata .srodata.*)
    }

    . = ALIGN(4K);
    erodata = .;
    sdata = .;
    .data : {
        *(.data .data.*)
        *(.sdata .sdata.*)
    }

    . = ALIGN(4K);
    edata = .;
    .bss : {
        *(.bss.stack)
        sbss = .;
        *(.bss .bss.*)
        *(.sbss .sbss.*)
    }

    . = ALIGN(4K);
    ebss = .;
    ekernel = .;
}
```



4、执行编译并检验

```
### 增加 -v 可以详细看出编译信息
cargo build -v
   Compiling myos v0.1.0 (myos)
     Running `/Users/Rust4fun/.rustup/toolchains/nightly-x86_64-apple-darwin/bin/rustc --crate-name myos --edition=2021 src/main.rs --error-format=json --json=diagnostic-rendered-ansi,artifacts,future-incompat --diagnostic-width=210 --crate-type bin --emit=dep-info,link -C panic=abort -C embed-bitcode=no -C debuginfo=2 -C metadata=c1bfe1fc6d2a0a03 -C extra-filename=-c1bfe1fc6d2a0a03 --out-dir /Users/Rust4fun/works/rust/myos/target/riscv64gc-unknown-none-elf/debug/deps --target riscv64gc-unknown-none-elf -C linker=riscv64-unknown-elf-ld -C incremental=/Users/Rust4fun/works/rust/myos/target/riscv64gc-unknown-none-elf/debug/incremental -L dependency=/Users/Rust4fun/works/rust/myos/target/riscv64gc-unknown-none-elf/debug/deps -L dependency=/Users/Rust4fun/works/rust/myos/target/debug/deps -Clink-arg=-Tsrc/linker.ld -Cforce-frame-pointers=yes -Clink-arg=-Map=target/output.map`
    Finished `dev` profile [unoptimized + debuginfo] target(s) in 0.46s
```

编译完成后可以使用 nm 命令或者查看 target/output.map 文件查看符号地址信息。

```
### 确认 _start 地址是 0x080200000 就对了
cargo nm
    Finished `dev` profile [unoptimized + debuginfo] target(s) in 0.03s
0000000080200000 A BASE_ADDRESS
0000000080200000 T _start
0000000080201000 T ebss
0000000080201000 T edata
0000000080201000 T ekernel
0000000080201000 T erodata
0000000080201000 T etext
0000000080201000 T sbss
0000000080201000 T sdata
0000000080200000 T skernel
0000000080201000 T srodata
0000000080200000 T stext
```



# Qemu 调试程序

使用 Qemu 执行内核（空跑）

由于 gcc 生成的是 elf 格式的程序，bootload 并不会去解析它。我们需要使用 objcopy 工具把 elf 转换成二进制机器码程序（去掉 elf 格式的头信息）。cargo-binutils 为我们提供的命令支持：

```
### 生成 myos.bin 
rust-objcopy --strip-all target/riscv64gc-unknown-none-elf/debug/myos -O binary target/riscv64gc-unknown-none-elf/debug/myos.bin

### Qemu 运行加载 myos.bin (运行之后是卡住的，我们的 _start 函数就一个 loop 空循环）
qemu-system-riscv64 -machine virt -bios default -nographic \
    -kernel target/riscv64gc-unknown-none-elf/debug/myos.bin
                        
### Qemu 退出组合键：Ctrl+a x
```

Qemu 也支持通过 gdb 调试，增加 -S -s 两个参数即可等待 gdb 连接进行单步调试。

```
### 使用 GDB 调试
riscv64-unknown-elf-gdb \
    -ex 'file target/riscv64gc-unknown-none-elf/debug/myos.bin' \
    -ex 'set arch riscv:rv64' \
    -ex 'target remote localhost:1234'

### gdb 进入后设置断点：0x080200000 即可调试 bootloader 程序
```

虽然 myos.bin 只有一个空跑的循环指令（ loop ），我们总算是起了个头。后面在此基础上，我们逐步增加串口打印，并实现 Hello world 输出。



本项目的源码地址：[myos/ch1-hello](https://gitee.com/rust4fun/myos/tree/master/ch1-hello)



参考文章：

* [rCore-Tutorial-Book 第三版](https://rcore-os.cn/rCore-Tutorial-Book-v3/chapter0/index.html)

* [Writing an OS in Rust](https://os.phil-opp.com/zh-CN/)