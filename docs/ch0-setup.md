## 开发环境搭建 - MacOS

大部分使用 MacOS 进行软件开发的同学，要么需要安装虚拟机套个 ubuntu，要么安装 Docker 进行嵌入式开发。在开发调试过程中，风扇总是高速转动（有点吵且慢）。其实 MacOS 对开发人员是蛮友好的，该有的命令基本都有。唯一不足的是文件系统不区分大小写，导致一些内核头文件识别异常，需要使用 hdiutil 工具制作一个镜像分区挂在到系统中。在 MacOS 上进行嵌入式开发环境的搭建通常涉及到安装交叉编译、调试器、编辑器（VSCode）、Qemu、交叉编译起等工具，下面分别介绍一下这些工具的安装方法。



1、Homebrew

MacOS 官方的软件管理程序，类似 ubuntu 的 apt-get 工具。安装命令如下：

```
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```



2、VSCode

**VSCode 是一款由微软开发且跨平台的免费源代码编辑器**，大部分程序员都认可是使用的代码编辑器。下载直接打开使用即可，下载地址：
https://code.visualstudio.com/



3、编译器以及安装交叉编译

苹果系统自带的编译器是 Xcode Command Line Tools，安装方法如下：

```
xcode-select --install
```

嵌入式交叉编译建议使用 musl gcc，安装方法参考：[MacOS 下 Musl Gcc 编译器构建](https://www.toutiao.com/article/7345501349540332051/?log_from=de878bc96a8ae_1710335801058)

Musl Gcc 工具链包含了 gdb、readelf 等工具，方便后续进行调试。



4、Qemu 安装

QEMU（Quick EMUlator）是一个开源的虚拟机监视器和模拟器，它可以模拟多个硬件架构，包括 x86、ARM、PowerPC、SPARC 等，并能运行多种操作系统。

开发过程中我们可以方便的使用 Qemu 进行编译调试。

```
## 使用 homebrew 安装
brew install qemu

## 安装后验证，确保版本在 8.0 版本以上
qemu-system-x86_64 --version
```



5、交叉编译起 GCC 安装

我们打算使用 Rust 写 Riscv64 平台的操作系统，从 Sifive 官网下载交叉编译起即可（支持Mac）。

下载地址是（选择 v2020.12 版本）：https://github.com/sifive/freedom-tools/releases



6、GNU 相关工具安装

由于 MacOS 的一些命令参数不兼容 Linux ，开发过程中直接进行源码编译会报命令参数错误。我们可以通过 Homebrew 来安装 GNU 相关工具，命令如下：

```
### 将安装 GNU 工具集的核心工具，如 ls、cp、mv、rm 等，以及 find 命令。
brew install coreutils
brew install findutils

### 设置环境变量
export PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"
export PATH="/usr/local/opt/findutils/libexec/gnubin:$PATH"

### 执行 ls 命令检查是否生效
ls --version
ls (GNU coreutils) 9.0
Copyright (C) 2021 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Written by Richard M. Stallman and David MacKenzie.
```

其他一些额外常见工具安装：

```
### 字符串查找
brew install gsed
export PATH="/usr/local/opt/gsed/libexec/gnubin:$PATH"

### windows 格式转成 linux 格式
brew install dos2unix
export PATH="/usr/local/opt/dos2unix/bin:$PATH"

### 制作 Fat32 文件系统（mkfs.vfat）
brew install dosfstools
export PATH="/usr/local/opt/dosfstools/sbin:$PATH"
```