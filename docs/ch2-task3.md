## Rust 写操作系统之线程（四）

Rust 写操作系统系列：[Rust 写操作系统之线程（三）](https://zhuanlan.zhihu.com/p/689957757)

前面两个章节分别讲了主动切换和定时器中断，这一章节我们来融合这两个章节的内容，使得我们的线程除了能支持主动切换外，还能支持抢占式调度。这一章节内容对原来的 switch 和 trap 都有较大修改，所以单独一个目录来存放代码：https://gitee.com/rust4fun/myos/tree/master/ch2-task1

前面虽然已经完成的线程主动切换 `__switch` 和中断函数 trap_handler 周期性执行，理论上我们只需要在 trap_handler 中断函数中调用`__switch` 函数即可。实际过程比这个要复杂一些，下面分别讲解融合过程中遇到的问题以及解决思路。在融合 trap 和 task 代码过程中第一次遇到使用 rust 写出来的 bug，也花费一点时间诊断调试了一下。这里稍微也提一下，在下面代码注释掉 `trap::init_trap();` 或者 `tasks::task_run();` 的情况下没问题，两个同时调用就会出现异常（重复打印 println! 中的内容，与预期流程不符）。使用 gdb 跟踪诊断最终发现栈溢出了，目前 rust 的对象以及 `println!` 内容都是存放在栈空间的，超过了 `4k` 空间导致栈异常了（也搜索了一下发现 rust 目前还没办法编译时指定栈大小并检测栈是否溢出了）。

~~~sh
#[no_mangle]
unsafe extern "C" fn rust_entry(_cpu_id: usize, _dtb: usize) {
    clear_bss();
    println!("+++++++++++++++++++++++++++++++++++++++++++++++++\n\n");
    println!("     Hello myos !!!\n\n");
    println!("+++++++++++++++++++++++++++++++++++++++++++++++++\n\n");

    trap::init_trap();
    tasks::task_run();
    sbi::shutdown(false);
}
~~~

### 线程和中断融合问题

1、中断如何保存现场到当前线程的 TaskContext 中

2、中断和线程初始化执行顺序问题

3、主动切换和中断抢占现场保存和还原问题

针对第一个问题， Riscv 有提供了一个 sscratch 寄存器用于存放 TaskContext 地址。我们只需要每次执行线程之前把 TaskContext 地址存储到 sscratch 寄存器中即可。sscratch 寄存器需要有特殊的指令读写，分别如下（读写、读、写三条指令）：

~~~sh
### 交换 sp 和 sscratch 内容
csrrw sp, sscratch, sp
### 把 sscratch 内容读到 t2 寄存器
csrr t2, sscratch
### 把 a0 内容写到 sscratch 寄存器
csrw sscratch, a0
~~~

针对第二个问题，中断和线程初始化顺序会产生问题的原因是，中断初始化之后就会触发中断进行线程切换，但是我们的线程相关的环境还没初始化导致异常。如果线程先初始化的话，就会立即进入循环调度线程模式，主线程不会获取到 CPU 执行中断相关初始化导致抢占调度功能无法使用。这个问题有两个解决方法：

* 添加一个主线程，用于执行中断初始化操作
* 先执行中断相关初始化，但最后不开启中断使能，等到线程切换时再使能

这里我们选择第二个解决方案，因为线程切换过程中我们不希望被中断。由于线程的切换是使用并修改了全局变量 `TASK_MANAGER` 所以这个过程（函数）是不可重入的，所以我们需要在切换的时候关闭中断，完成后切换再开启中断，刚好符合第二个方案的要求。

关于第三个问题，中断处理函数中保存了 TaskContext 了，如果在中断继续调用 __switch 保存当前线程的 TaskContext 的话，就会出现执行异常（中断上下文并不是我们想要的现场）。要解决这个问题也比较简单，中断过程我们不调用 switch 函数进行切换而是新写一个函数选择下一个要运行线程并返回其 TaskContext 指针。然后 trap_handler 函数返回要执行线程的 TaskContext 提供给汇编进行现场还原。

### 线程上下文 TaskContext

为了能够支持中断上下文切换，我们对所有寄存器进行了保存，因此扩充了 TaskContext 结构体，包含 32 个寄存器以及 sstatus（状态寄存器） 和 sepc （中断返回时的 PC 地址）。其中 id 是调试用的，初始化时给线程一个数字排序方便诊断是哪个线程。

~~~sh
#[derive(Copy, Clone, Debug)]
#[repr(C)]
pub struct TaskContext {
    pub x: [usize; 32],
    pub sstatus: usize,
    pub sepc: usize,
    pub id: usize,
}
~~~

### 现场恢复

我们按照执行顺序来讲一下现场的恢复与保存。为什么要先讲恢复呢，有两个原因：

* 运行中断前需要先设置 `sscratch` 寄存器，这个是在恢复现场时执行的
* 我们前面讲过在切换的时候需要开启中断，这个是在恢复现场时执行的

在 trap.S 中定义 `__restore` 函数用来现场恢复的。这里使用了一个技巧，函数调用约定是使用 a0 寄存器作为函数第一个参数，函数返回值也是使用 a0 寄存器。下面`__restore` 代码中 a0 寄存器对于中断处理 trap_handler 来说是返回值，对于 switch.S 中的 `__switch` 来说是调用的第一个参数。`__restore` 主要完成步骤如下：

* 从 TaskContext （a0 地址）中还原 sstatus 和 sepc 寄存器
* 从 TaskContext （a0 地址）中还原通用寄存器 x[0..31]
* 调用 sret 从中断执行返回到 sepc 地址执行程序

备注：使用 sp 当作临时寄存器使用，没有使用 a0 纯粹为了调用 `rept` 方便一些。

~~~sh
__restore:
    ### 现场恢复是现场保存到逆过程，先恢复 sstatus 和 SEPC
    mv sp, a0
    ld t0, 32*8(sp)
    ld t1, 33*8(sp)
    csrw sstatus, t0
    csrw sepc, t1
    ### 设置 scratch 寄存器给下次中断使用
    csrw sscratch, a0
    ### 还原其他通用寄存器（分两步，SP 最后还原）
    ld x1, 1*8(sp)
    .set n, 3
    .rept 29
        LOAD_GP %n
        .set n, n+1
    .endr
    ### 还原 SP 寄存器并返回中断处理
    ld sp, 2*8(sp)
    sret
~~~

`sret` 和 `ret` 指令的区别在于，`ret` 返回后 PC 加载的是 ra 寄存器的值，而 `sret` 返回之后，PC 加载的是 sepc 寄存器的值。除此之外 `sret` 还多执行了以下两个事（效率更低）：

* 根据 `sstatus` 寄存器 SPP 字段判断是否返回到用户态
* 根据 `sstatus` 寄存器 SPIE 字段判断是否开启中断

因此我们在调用 `__switch`（`__restore`） 之前调用以下函数设置 SPIE 以及 SSP 。

~~~sh
/* 返回后开启中断 */
riscv::register::sstatus::set_spie();   
/* 返回后仍处理特权模式 */
riscv::register::sstatus::set_spp(riscv::register::sstatus::SPP::Supervisor);
~~~

### 线程切换（主动）

![riscv-sstatus](riscv-sstatus.png)

线程切换和中断现场保存有一点区别在于，中断的现场保存部分操作由 CPU 硬件执行，而线程切换都是由软件执行的。为了使用共同的现场恢复过程，我们在线程切换得有一些额外的处理。根据上面 `sret` 的返回过程，保存当前 context 时设置 sstatus 的 SPP 和 SPIE 标志位为 1。需要在 `__switch` 函数中增加以下代码：

~~~sh
    ### 保存 sstatus 值并设置 SPP-bit8 和 SPIE-bit5 标志位
    li t1, 0x120
    csrr t0, sstatus
    or t0, t0, t1
    sd t0, 32*8(a0)
~~~

上述代码读出 sstatus 寄存器值并与 0x120 或之后存储到 TaskContext 中的 sstatus 变量中。其中 sstatus 变量在 TaskContext 结构体偏移 32 * 8 字节地址； 0x120 是 bit5 和 bit8 设置为 1 时的值。除了需要修改 __switch 代码之外，我们还要初始化 TaskContext 的时候把 spec 设置为函数指针地址并把 sstatus 值设置为 0x120 ，这样能够使得线程第一次切换时正确执行：

~~~sh
impl TaskContext {
    pub fn init(id: usize, sp: usize, entry: usize) -> Self {
        let mut ctx = Self {
            x: [0; 32],
            sstatus: 0,
            sepc: entry,
            id: id,
        };
        /* RA 和 SP 寄存器 */
        ctx.x[2] = sp;
        /* SPP 和 SPIE 设置为 1 */
        ctx.sstatus = 0x120;
        return ctx;
    }
}
~~~

`__switch` 函数在保存上下文除了正常保存寄存器和 sstatus 外，sepc 的保存也需要进行一些特殊处理。主动切换的线程执行 `task_yield` 函数进行切换，下次恢复现场时肯定是希望从 `task_yield` 函数继续运行的。`__switch` 保存现场我们能直接把当前 PC 地址保存到 sepc 中么？ 答案是不行的，这样下次切换回来的时候就立即又跳到 `__switch` 中执行现场恢复，进入死循环了。我们应该把 `__switch` 函数 ret 返回指令的地址加载到 sepc 中，这样下次调度到该线程时就可以立即从 `__switch` 函数一路返回到 `task_yield` 的调用函数继续执行该线程的任务了。这段代码实现使用了 `auipc` 指令和汇编伪代码 `pcrel_hi` 、`pcrel_lo` 来加载符号的地址。

~~~sh
    ### 保存 sepc 地址
1:  auipc t1, %pcrel_hi(__epc_addr)
    addi t1, t1, %pcrel_lo(1b)
    sd t1, 33*8(a0)
    ########################################
    # 调用 trap.S __restore 还原 next_context
    ########################################
    mv a0, a1
    call __restore

    # current_context 下次调度时从这里开始执行
__epc_addr:
    ret
~~~

上述已经把 switch.S 代码基本讲解差不多了，完整的 switch.S 代码就不贴了，可以直接看源文件：https://gitee.com/rust4fun/myos/blob/master/ch2-task1/src/switch.S

除了上述现场恢复和切换需要调整之外，我们还需要在主动切换函数 `sched` 开始时添加一个禁用中断的指令，因为`task_man` 相关处理不能被中断。

~~~sh
    /* 线程调度（当前线程 ID+1 即可） */
    pub fn sched(&self) {
        /* 禁用中断, __switch 恢复时使能中断 */
        unsafe { riscv::register::sstatus::clear_sie(); }
        /* 获取当前任务 TaskContex 信息 */
        let mut task_man = self.inner.borrow_mut();
        ...
        drop(task_man);
        /* 执行切换 */
        unsafe {
            __switch(curr_context, next_context);
        }
    }
~~~



### 中断强制切换线程

上一章节中断时直接把现场保存到当前线程的栈上，这一章我们利用 `sscratch`寄存器，把现场保存到当前线程的 TaskContext 中。而且还要修改 trap_handler 函数签名使其接收当前线程 TaskContext 并返回下一个执行线程的 TaskContext，签名如下：

~~~c
#[no_mangle]
pub extern "C" fn trap_handler(ctx : *const TaskContext) -> *const TaskContext 
~~~

下面代码就是常规的保存寄存器/sstatus和sepc。特别需要说明的就是使用 SP 寄存器当作 TaskContext 临时交换使用，最后还需要在从 `sscratch` 寄存器读出真正的 SP 寄存器再保存一次。

~~~sh
__alltraps:
    ### scratch 保存运行线程的 TaskContext 地址; 使用 SP 交换
    csrrw sp, sscratch, sp
    ### 保存 x1~x31 寄存器到 TaskContext
    .set n, 1
    .rept 31
        SAVE_GP %n
        .set n, n+1
    .endr
    ### 保存中断状态寄存器 sstatus 和 SEPC / SP
    csrr t0, sstatus
    csrr t1, sepc
    sd t0, 32*8(sp)
    sd t1, 33*8(sp)
    csrr t2, sscratch
    sd t2, 2*8(sp)
    ### 调用到 rust 函数处理中断信息
    mv a0, sp
    mv sp, t2
    call trap_handler
~~~

`trap_handler` 相对于上个章节改动不大，就是根据接口要求在定时器中断处理函数中调用 `tasks::task_next()` 函数获取下个线程的 TaskContext 指针。这里也没有太多需要讲解的地方，代码如下：

~~~sh
#[no_mangle]
pub extern "C" fn trap_handler(ctx : *const TaskContext) -> *const TaskContext {
    let mut next = ctx;
    /* 读取中断原因以及相应的中断值 */
    let scause = riscv::register::scause::read();
    let stval = riscv::register::stval::read();
    match scause.cause() {
        /* 定时器中断处理 */
        Trap::Interrupt(Interrupt::SupervisorTimer) => {
            /* 读取当前定时器计数 */
            let curr = riscv::register::time::read() as u64;
            /* 设置下一次中断计数值: 10ms */
            sbi_rt::set_timer(curr + CLOCK_FREQ/TICKS_PER_SEC);
            // 调度到下一个任务
            next = crate::tasks::task_next(); 
        }
        _ => {
            panic!("Unsupported trap {:?}, stval = {:#x},!", scause.cause(), stval);
        }
    }
    next
}
~~~

而 task_next 函数跟 sched 函数有点类似，选择下一个线程返回其 TaskContext 指针。这里需要注意的一点是 Rust 中关于引用 `&` 和指针 `*` 之间的差异。引用是编译器自动管理并进行推导的，而指针则需要程序员自行管理（容易出现也指针问题）。下面代码如果使用引用的话则会由于不满足规则编译不过，所以需要转换成指针告诉编译器你不用管了(^_^)。

~~~sh
impl TaskManager {
    pub fn next(&self) -> * const TaskContext {
        /* 获取当前任务 TaskContex 信息 */
        let mut task_man = self.inner.borrow_mut();
        let curr_idx = task_man.current_task;
        /* 获取下一个任务 TaskContex 信息 */
        let mut next_idx = curr_idx + 1;
        if next_idx >= task_man.task_context.len() {
            next_idx = 0;
        }
        task_man.current_task = next_idx;
        let next_context = &task_man.task_context[next_idx] as *const TaskContext;
        /* RefCell borrow 之后需要手动 drop */
        drop(task_man);
        return next_context;
    }
}
~~~



### 总结

这里为止，我们已经完成了整个线程的被动以及主动式切换实现。唯一的限制就是线程无法动态创建和销毁，毕竟现在还没有实现堆内存管理，等后续实现了之后再回过头了继续实现线程的动态管理。这里再简要介绍一下项目文件代码：

* main.rs :  定义程序入口 `_start` 设置程序执行环境并初始化中断和线程。
* tasks.rs :  定义了线程相关函数，包括 sched、next、run_first 几个线程切换函数。
* trap.rs ：定义 trap_handler 时间中断处理函数
* switch.S : 定义线程主动切换的现场保存处理函数
* trap.S : 定义中断的现场保存和恢复函数

完整代码大家可以跟着项目看，执行该项目下的 `run.sh` 脚本即可看到以下输出：

![output](ch2-task3-output.png)



