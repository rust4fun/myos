/*********************************************
 * 线程数据结构，保存线程的运行状态
 * 当前先定义个函数指针，表示这个线程需要执行的任务
 **********************************************/
struct TaskControlBlock {
    pub func : fn(),
}

/* 定义线程创建方法 */
impl TaskControlBlock {
    pub fn new(func: fn()) -> Self {
        Self {
            func: func,
        }
    }
}

/*********************************************
 * 线程管理器，用于调度线程 
 * 备注：
 *  由于我们还没有堆内存，只能用栈内存和全局变量
 *  所以这里不能使用 Vec 数组，只能写死管理 5 个线程
 **********************************************/
struct TaskManager {
    tasks_context : [TaskControlBlock; 5],
}

/* 定义线程管理器创建方法 */
impl TaskManager {
    pub fn new() -> Self {
        Self {
            tasks_context: [
                TaskControlBlock::new(task1),
                TaskControlBlock::new(task2),
                TaskControlBlock::new(task3),
                TaskControlBlock::new(task4),
                TaskControlBlock::new(task5),
            ],
        }
    }
}

/*********************************************
 * 线程任务函数 
 * 每个线程只打印一下信息即可
 **********************************************/
fn task1() {
    println!("this is task1");
}
/* 线程2 */
fn task2() {
    println!("this is task2");
}
/* 线程3 */
fn task3() {
    println!("this is task3");
}
/* 线程4 */
fn task4() {
    println!("this is task4");
}
/* 线程5 */
fn task5() {
    println!("this is task5");
}


/*********************************************
 * 进入线程调度管理
 **********************************************/
pub fn task_run() {
    /* 创建线程管理器 */
    let mut tm = TaskManager::new();
    /* 每个线程轮流执行过去 */
    for mut task in tm.tasks_context {
        (task.func)();
    }
}