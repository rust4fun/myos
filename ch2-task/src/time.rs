use riscv::register::time;

// 定义时钟频率为 12.5 MHz
const CLOCK_FREQ: u64 = 12500000;
// 定义一秒有多少个时钟中断，每个中断 10ms
const TICKS_PER_SEC: u64 = 100;
// 定义变量 毫秒
const MS_PER_SECOND: u64 = 1000;
// 定义变量 微妙
const US_PER_SECOND : u64 = 1000 * 1000;


/* 获取当前时间计数器的值 */
pub fn current_time() -> u64 {
    time::read() as u64
}

/* 获取当前时间：毫秒单位 */
pub fn current_ms() -> u64 {
    current_time() / (CLOCK_FREQ / MS_PER_SECOND)
}

/* 获取当前时间：微秒单位 */
pub fn current_us() -> u64 {
    current_time() / (CLOCK_FREQ / MS_PER_SECOND)
}

pub fn set_next_time() {
    sbi_rt::set_timer(current_time() + CLOCK_FREQ / TICKS_PER_SEC);
}


pub fn init_time() {
    sbi_rt::set_timer(0);
}
