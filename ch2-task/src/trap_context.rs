use riscv::register::sstatus::{Sstatus, self};

/** 用于保存寄存器以及 */
#[repr(C)]
pub struct TrapContext {
    pub x: [usize; 32],
    pub sstatus: Sstatus,
    pub sepc: usize,
}

impl TrapContext {
    pub fn set_sp(&mut self, sp: usize) {
        self.x[2] = sp;
    }

    pub fn init(entry:usize, sp: usize) -> Self {
        let sstatus_val = sstatus::read();
        let mut cx = Self {
            x: [0; 32],
            sstatus: sstatus_val,
            sepc: entry,
        };
        cx.set_sp(sp);
        return cx;
    }
}
