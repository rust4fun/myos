use core::fmt::{self, Write};

struct Stdout;

impl Write for Stdout {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        for c in s.chars() {
            crate::sbi::console_putchar(c as usize);
        }
        Ok(())
    }
}

pub fn __print(args: fmt::Arguments) {
    Stdout.write_fmt(args).unwrap();
}

/// 定义 print 宏打印消息到串口
#[macro_export]
macro_rules! print {
    ($($arg:tt)*) => {
        $crate::console::__print(format_args!($($arg)*));
    }
}

// 定义 println 宏，增加换行输出
#[macro_export]
macro_rules! println {
    () => { $crate::print!("\n") };
    ($($arg:tt)*) => {
        $crate::console::__print(format_args!("{}\n", format_args!($($arg)*)));
    }
}




