
use core::arch::global_asm;
use riscv::register::{
    mtvec::TrapMode,
    scause::{
        self,
        Trap,
        Exception,
        Interrupt,
    },

};

pub const CLOCK_FREQ: u64 = 12500000;
pub const TICKS_PER_SEC: u64 = 100;

global_asm!(include_str!("trap.S"));

pub fn init_trap() {
    extern "C" { fn __alltraps(); }
    unsafe {
        // 设置中断处理函数 
        riscv::register::stvec::write(__alltraps as usize, TrapMode::Direct);
    }

    // 调用 sbr_rt 设置定起始比较器触发中断
    sbi_rt::set_timer(0);
    unsafe {
        // 启动时钟中断
        riscv::register::sie::set_stimer();
        // 启用中断
        riscv::register::sstatus::set_sie();
    }
}



#[no_mangle]
pub extern "C" fn trap_handler() {
    /* 读取中断原因以及相应的中断值 */
    let scause = riscv::register::scause::read();
    let stval = riscv::register::stval::read();
    match scause.cause() {
        /* 定时器中断处理 */
        Trap::Interrupt(Interrupt::SupervisorTimer) => {
            /* 读取当前定时器计数 */
            let curr = riscv::register::time::read() as u64;
            /* 设置下一次中断计数值: 10ms */
            sbi_rt::set_timer(curr + CLOCK_FREQ/TICKS_PER_SEC);
            /* 打印当前计数器的值 */
            println!("[Intterupt] Current Time: {}", curr);
        }
        _ => {
            panic!("Unsupported trap {:?}, stval = {:#x}!", scause.cause(), stval);
        }
    }
}

